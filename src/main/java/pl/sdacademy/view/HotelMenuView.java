package pl.sdacademy.view;

import pl.sdacademy.model.Guest;
import pl.sdacademy.model.Room;
import pl.sdacademy.service.HotelService;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class HotelMenuView {

    private static final int MENU_EXIT = 0;
    private static final int MENU_SHOW_ALL_ROOMS = 1;
    private static final int MENU_SHOW_AVAILABLE_ROOMS = 2;
    private static final int MENU_MAKE_RESERVATION = 3;
    private static final int MENU_FREE_ROOM = 4;
    private static final int MENU_SHOW_DIRTY_ROOMS = 5;
    private static final int MENU_CLEAN_ROOM = 6;
    private static final int MENU_SHOW_OCCUPIED_ROOMS = 7;

    private HotelService hotelService;
    private static Scanner scanner = new Scanner(System.in);

    public HotelMenuView(HotelService hotelService) {
        this.hotelService = hotelService;
    }

    public void showPage() {
        int input = 0;

        do {
            printMenu();
            input = getInt();

            switch(input) {
                case MENU_SHOW_ALL_ROOMS:
                    printAllRoomsInfo();
                    break;
                case MENU_SHOW_AVAILABLE_ROOMS:
                    printAvailableRoomsInfo();
                    break;
                case MENU_MAKE_RESERVATION:
                    showReservationPanel();
                    break;
                case MENU_FREE_ROOM:
                    showFreeRoomPanel();
                    break;
                case MENU_SHOW_DIRTY_ROOMS:
                    printDirtyRooms();
                    break;
                case MENU_CLEAN_ROOM:
                    showCleaningRoomPanel();
                    break;
                case MENU_EXIT:
                    System.out.println("Exiting app...");
                    break;
                case MENU_SHOW_OCCUPIED_ROOMS:
                    printOccupiedRooms();
                    break;

                    default:
                        System.out.println("Nieprawidłowy numer operacji!");
            }
            System.out.println("\n");
        } while(input != MENU_EXIT);
    }

    private void printMenu() {
        System.out.println("..:::HOTEL SERVICE MENU:::..");
        System.out.println("1. Wyświetl listę pokoi.");
        System.out.println("2. Wyświetl listę dostępnych pokoi.");
        System.out.println("3. Zarezerwój pokój.");
        System.out.println("4. Zwolnij pokój.");
        System.out.println("5. Wyswietl listę pokoi do posprzątania.");
        System.out.println("6. Posprzątaj pokój.");
        System.out.println("7. Pokaż listę zajętych pokoi.");
        System.out.println("0. Wyjście.");
    }

    private void printAllRoomsInfo() {
        System.out.println("-----------------");
        System.out.println("WSZYSTKIE POKOJE:");
        for (Room room : hotelService.getAllRooms()) {
            System.out.println("Pokój nr: " + room.getNumber() + " / " +
                    (room.isAvailable() ? "dostępny" : "niedostępny"));
        }
        System.out.println("=================");
    }

    private void printAvailableRoomsInfo() {
        System.out.println("-----------------");
        System.out.println("DOSTĘPNE POKOJE:");
        for (Room room : hotelService.getAllAvailableRooms()) {
            System.out.println(room);
        }
        System.out.println("=================");
    }

    private void printDirtyRooms() {
        System.out.println("-----------------------");
        System.out.println("POKOJE DO POSPRZĄTANIA:");
        for (Room room : hotelService.getDirtyRooms()) {
            System.out.print(room.getNumber() + "  ");
        }
        System.out.println("\n=======================");
    }

    private void printOccupiedRooms() {
        System.out.println("-----------------------");
        System.out.println("ZAJĘTE POKOJE:");
        for (Room room : hotelService.getAllOccupiedRooms()) {
            System.out.println("Pokój nr " +room.getNumber() + " zajęty do: " + room.getCheckOutDate());
        }
        System.out.println("\n=======================");
    }

    private void showReservationPanel() {
        int roomNumber, days;
        System.out.println("------------------");
        System.out.println("REZERWACJA POKOJU:");
        System.out.println("Podaj numer pokoju, który zarezerwować: ");
        roomNumber = getInt();

        System.out.println("Podaj liczbę dni:");
        days = getInt();

        List<Guest> guests = getGuestListFromUser();

        if (hotelService.makeReservation(roomNumber, days, guests)) {
            System.out.println("Rezerwacja powiodła się.");
        } else {
            System.out.println("Rezerwacja nie powiodła się.");
        }
        System.out.println("==================");
    }

    private void showFreeRoomPanel() {
        int roomNumber;

        System.out.println("Podaj numer pokoju, który zwolnić: ");
        roomNumber = getInt();

        if (hotelService.freeRoom(roomNumber)) {
            System.out.println("Udało się zwolnić pokój.");
        } else {
            System.out.println("Nie udało się zwolnić pokoju.");
        }
    }

    private void showCleaningRoomPanel() {
        int roomNumber;
        System.out.println("Podaj numer pokoju do posprzątania: ");
        roomNumber = getInt();
        hotelService.cleanRoom(roomNumber);
        System.out.println("Posprzątano pokój nr " + roomNumber);
    }

    private int getInt() {
        int input = 0;

        while(true) {
            try {
                input = scanner.nextInt();
                break;
            } catch(InputMismatchException e) {
                System.out.println("Podaj liczbę!");
                scanner.nextLine();
            }
        }

        return input;
    }

    private List<Guest> getGuestListFromUser() {
        int numberOfGuests = 0;
        String userInput;
        List<Guest> guestList = new ArrayList<>();

        System.out.println("Podaj liczbę gości: ");
        numberOfGuests = getInt();

        for (int i = 0; i < numberOfGuests; i++) {
            System.out.println("Podaj imię, nazwisko i datę urodzenia (odzielone spacjami):");
            userInput = scanner.nextLine();
            String[] userData = userInput.split(" ");
            if (userData.length != 3) {
                System.out.println("Zła liczba parametrów, powtórz!");
                i--;
            } else {
                LocalDate date;
                try {
                    date = LocalDate.parse(userData[2]);
                    guestList.add(new Guest(userData[0],userData[1],date));
                } catch (DateTimeParseException e) {
                    System.out.println("Nie poprawny format daty (YYYY-MM-DD), powtórz!");
                    i--;
                }
            }
        }

        return guestList;
    }
}
