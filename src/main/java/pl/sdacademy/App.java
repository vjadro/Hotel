package pl.sdacademy;

import pl.sdacademy.model.Hotel;
import pl.sdacademy.service.HotelService;
import pl.sdacademy.view.HotelMenuView;

public class App 
{
    public static void main( String[] args )
    {
        HotelService hotelService = new HotelService(new Hotel());

        HotelMenuView hotelMenuView = new HotelMenuView(hotelService);

        hotelMenuView.showPage();
    }
}
