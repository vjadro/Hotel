package pl.sdacademy.service;

import pl.sdacademy.model.BookingProperties;
import pl.sdacademy.model.Guest;
import pl.sdacademy.model.Hotel;
import pl.sdacademy.model.Room;
import pl.sdacademy.utils.ConfigFileUtil;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;

public class HotelService {

    private Hotel hotel;

    public HotelService(Hotel hotel) {
        this.hotel = hotel;
    }

    public List<Room> getAllRooms() {
        return hotel.getAllRooms();
    }

    public List<Room> getAllAvailableRooms() {
        List<Room> availableRooms = new ArrayList<>();

        for (Room room : hotel.getAllRooms()) {
            if (room.isAvailable() && (room.isClean()))
                availableRooms.add(room);
        }

        return availableRooms;
    }

    public List<Room> getAllOccupiedRooms() {
        List<Room> occupiedRooms = new ArrayList<>();

        for (Room room : hotel.getAllRooms()) {
            if (!room.isAvailable()) {
                occupiedRooms.add(room);
            }
        }

        return occupiedRooms;
    }

    public BigDecimal makeReservation2(BookingProperties bookingProperties, int days) {
        for (Room room : hotel.getAllRooms()) {
            if ((room.getNumber() == bookingProperties.getRoomNumber()) && (room.isAvailable())) {

                boolean anyAdult = false;

                for (Guest guest : bookingProperties.getGuestList()) {
                    if (Period.between(guest.getDateOfBirth(), LocalDate.now()).getYears() >= 18) {
                        anyAdult = true;
                        break;
                    }
                }

                if (anyAdult && (room.getNumOfPeople() >= bookingProperties.getGuestList().size()) && room.isClean()) {
                    room.setAvailable(false);
                    room.setGuestList(bookingProperties.getGuestList());
                    room.setCheckInDate(LocalDate.now());
                    room.setCheckOutDate(LocalDate.now().plusDays(days));

                    ConfigFileUtil config = new ConfigFileUtil();
                    config.loadConfig();

                    String promoCode = config.getProperty("discountcode");
                    int feedingPrice = Integer.parseInt(config.getProperty(bookingProperties.getFeedOption().toString()));
                    float discount = promoCode.equals(bookingProperties.getDiscountCode()) ? 0.20f : 0;
                    double totalPrice = (1+discount) * (room.getPrice().floatValue() +
                                                        feedingPrice * bookingProperties.getGuestList().size());
                    return new BigDecimal(totalPrice).setScale(2, BigDecimal.ROUND_HALF_UP);
                }

                return new BigDecimal(0);
            }
        }
        return new BigDecimal(0);
    }

    public boolean makeReservation(int number, int days, List<Guest> guestList) {
        for (Room room : hotel.getAllRooms()) {
            if ((room.getNumber() == number) && (room.isAvailable())) {

                boolean anyAdult = false;

                for (Guest guest : guestList) {
                    if (Period.between(guest.getDateOfBirth(), LocalDate.now()).getYears() >= 18) {
                        anyAdult = true;
                        break;
                    }
                }

                if (anyAdult && (room.getNumOfPeople() >= guestList.size()) && room.isClean()) {
                    room.setAvailable(false);
                    room.setGuestList(guestList);
                    room.setCheckInDate(LocalDate.now());
                    room.setCheckOutDate(LocalDate.now().plusDays(days));
                    return true;
                }

                return false;
            }
        }
        return false;
    }

    public boolean freeRoom(int number) {
        for (Room room : hotel.getAllRooms()) {
            if ((room.getNumber() == number) && (!room.isAvailable())) {
                room.setClean(false);
                room.setAvailable(true);
                room.getGuestList().clear();
                return true;
            }
        }
        return false;
    }

    public void cleanRoom(int roomNumber) {
        for (Room room : hotel.getAllRooms()) {
            if (room.getNumber() == roomNumber) {
                room.setClean(true);
                return;
            }
        }
    }

    public List<Room> getDirtyRooms() {
        List<Room> dirtyRooms = new ArrayList<>();

        for (Room room : hotel.getAllRooms()) {
            if (!room.isClean())
                dirtyRooms.add(room);
        }
        return dirtyRooms;
    }
}
