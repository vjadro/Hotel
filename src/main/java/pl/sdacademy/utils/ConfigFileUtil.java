package pl.sdacademy.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class ConfigFileUtil {

    private Properties prop = new Properties();

    public void loadConfig() {
        try {
            // load a properties file for reading
            prop.load(new FileInputStream("config.properties"));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public String getProperty(String propName) {
        return prop.getProperty(propName);
    }
}
