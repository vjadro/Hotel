package pl.sdacademy.model;

import java.util.List;

public class BookingProperties {
    private int roomNumber;
    private FeedOption feedOption;
    private String discountCode;
    private List<Guest> guestList;

    public BookingProperties(int roomNumber, FeedOption feedOption, List<Guest> guestList) {
        this.roomNumber = roomNumber;
        this.feedOption = feedOption;
        this.guestList = guestList;
        discountCode = "";
    }

    public BookingProperties(int roomNumber, FeedOption feedOption, List<Guest> guestList, String discountCode) {
        this(roomNumber,feedOption,guestList);
        this.discountCode = discountCode;
    }

    public int getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(int roomNumber) {
        this.roomNumber = roomNumber;
    }

    public FeedOption getFeedOption() {
        return feedOption;
    }

    public void setFeedOption(FeedOption feedOption) {
        this.feedOption = feedOption;
    }

    public String getDiscountCode() {
        return discountCode;
    }

    public void setDiscountCode(String discountCode) {
        this.discountCode = discountCode;
    }

    public List<Guest> getGuestList() {
        return guestList;
    }

    public void setGuestList(List<Guest> guestList) {
        this.guestList = guestList;
    }
}
