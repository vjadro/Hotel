package pl.sdacademy.model;

import java.time.LocalDate;

public class Guest {
    private String fName;
    private String lName;
    private LocalDate dateOfBirth;

    public Guest(String fName, String lName, LocalDate dateOfBirth) {
        this.fName = fName;
        this.lName = lName;
        this.dateOfBirth = dateOfBirth;
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    @Override
    public String toString() {
        return "Guest{" +
                "fName='" + fName + '\'' +
                ", lName='" + lName + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                '}';
    }
}
