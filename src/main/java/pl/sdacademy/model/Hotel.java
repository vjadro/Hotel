package pl.sdacademy.model;

import java.util.ArrayList;
import java.util.List;

public class Hotel {

    private List<Room> listOfRooms = new ArrayList<>();

    public Hotel() {
        listOfRooms.add(new Room(2, 5, true, RoomStandard.STANDARD));
        listOfRooms.add(new Room(101, 2, true, RoomStandard.STANDARD));
        listOfRooms.add(new Room(102, 4, true, RoomStandard.RODZINNY));
        listOfRooms.add(new Room(103, 2, true, RoomStandard.STANDARD));
        listOfRooms.add(new Room(200, 6, true, RoomStandard.STANDARD));
        listOfRooms.add(new Room(201, 4, true, RoomStandard.RODZINNY));
        listOfRooms.add(new Room(300, 3, true, RoomStandard.APARTAMENT));
        listOfRooms.add(new Room(301, 2, true, RoomStandard.APARTAMENT));

        System.out.println(listOfRooms.get(0).getPrice());
        System.out.println(listOfRooms.get(7).getPrice());
    }

    public List<Room> getAllRooms() {
        return listOfRooms;
    }
}
