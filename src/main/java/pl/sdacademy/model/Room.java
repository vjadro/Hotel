package pl.sdacademy.model;

import pl.sdacademy.utils.ConfigFileUtil;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Room {

    private int number;
    private int numOfPeople;
    private boolean hasBathroom;
    private boolean isAvailable;
    private boolean isClean;
    private LocalDate checkInDate;
    private LocalDate checkOutDate;
    private RoomStandard standard;
    private BigDecimal price;
    private FeedOption feedOption;
    private List<Guest> guestList = new ArrayList<>();

    public Room(int number, int numOfPeople, boolean hasBathroom, RoomStandard standard) {
        this.number = number;
        this.numOfPeople = numOfPeople;
        this.hasBathroom = hasBathroom;
        this.standard = standard;
        isAvailable = true;
        isClean = true;

        setPrice();
    }

    private void setPrice() {
        ConfigFileUtil config = new ConfigFileUtil();
        config.loadConfig();
        int defaultPrice = Integer.parseInt(config.getProperty("defaultprice"));
        int floor = number / 100;

        float p = 0;

        switch (standard) {
            case STANDARD:
                p = 1.0f;
                break;
            case RODZINNY:
                p = 1.5f;
                break;
            case APARTAMENT:
                p = 2.0f;
                break;
        }

        price = new BigDecimal(p * defaultPrice * (1 + (floor * 0.05)));
        price = price.setScale(2, BigDecimal.ROUND_HALF_UP);
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getNumOfPeople() {
        return numOfPeople;
    }

    public void setNumOfPeople(int numOfPeople) {
        this.numOfPeople = numOfPeople;
    }

    public boolean isHasBathroom() {
        return hasBathroom;
    }

    public void setHasBathroom(boolean hasBathroom) {
        this.hasBathroom = hasBathroom;
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public void setAvailable(boolean available) {
        isAvailable = available;
    }

    public List<Guest> getGuestList() {
        return guestList;
    }

    public void setGuestList(List<Guest> guestList) {
        this.guestList = guestList;
    }

    public boolean isClean() {
        return isClean;
    }

    public void setClean(boolean clean) {
        isClean = clean;
    }

    public LocalDate getCheckInDate() {
        return checkInDate;
    }

    public void setCheckInDate(LocalDate checkInDate) {
        this.checkInDate = checkInDate;
    }

    public LocalDate getCheckOutDate() {
        return checkOutDate;
    }

    public void setCheckOutDate(LocalDate checkOutDate) {
        this.checkOutDate = checkOutDate;
    }

    public RoomStandard getStandard() {
        return standard;
    }

    public void setStandard(RoomStandard standard) {
        this.standard = standard;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public FeedOption getFeedOption() {
        return feedOption;
    }

    public void setFeedOption(FeedOption feedOption) {
        this.feedOption = feedOption;
    }

    @Override
    public String toString() {
        return "Room{" +
                "number=" + number +
                ", numOfPeople=" + numOfPeople +
                ", hasBathroom=" + hasBathroom +
                ", isAvailable=" + isAvailable +
                ", standard=" + getStandard() +
                ", price=" + getPrice() +
                '}';
    }
}
